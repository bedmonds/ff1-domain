namespace NES.Games.FinalFantasy.Tests.Roms.Usa {
    using Xunit;

    public class WeaponTests {
        [Theory,
        InlineData(0, "Wooden{Nunchuck}", 12, 10, EffectSprite.Nunchuck, Colour.Gold), // Wooden Nunchucks
        InlineData(37, "Katana{Space}", 33, 30, EffectSprite.Knife, Colour.Gold), // Katana
        InlineData(39, "Masmune", 56, 10, EffectSprite.Scimitar, Colour.LightGrey), // Masmune
        ]
        public void TestWeaponStats(int idx, string name, int dmg, int crit, EffectSprite sprite, Colour colour) {
            var rom = Helper.USAFromEnv();
            var wpn = rom.Weapons[idx];

            Assert.Equal(name, wpn.Name);
            Assert.Equal(dmg, wpn.Stats.Damage);
            Assert.Equal(crit, wpn.Stats.CriticalHitChance);
            Assert.Equal(sprite, wpn.Stats.Animation.Sprite);
            Assert.Equal(colour, wpn.Stats.Animation.Colour);
        }
    }
}

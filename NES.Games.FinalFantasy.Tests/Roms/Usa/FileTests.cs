﻿namespace NES.Games.FinalFantasy.Tests.Roms.Usa {
    using System.Linq;
    using Xunit;

    public class FileTests {
        [Fact]
        public void ValidEnvironmentROM() {
            Assert.True(Helper.USAFromEnv().Valid);
        }
    }
}

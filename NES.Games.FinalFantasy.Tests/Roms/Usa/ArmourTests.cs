namespace NES.Games.FinalFantasy.Tests.Roms.Usa {
    using Xunit;

    public class ArmourTests {
        [Theory,
        InlineData(0, "Cloth{Space}{Space}", 2, 1, Element.None, 0x00), // Cloth
        InlineData(39, "ProRing", 1, 8, Element.Death, 0x00), // ProRing
        InlineData(31, "Ribbon{Space}{Space}", 1, 1, Element.All, 0x00), // Ribbon
        InlineData(37, "Power{Space}{Gauntlet}", 3, 6, Element.None, 0x37), // Ribbon
        ]
        public void TestArmourStats(
            int idx,
            string name,
            int weight,
            int absorb,
            Element resists,
            byte castOnUse
        ) {
            var rom = Helper.USAFromEnv();
            var arm = rom.Armour[idx];

            Assert.Equal(name, arm.Name);
            Assert.Equal(weight, arm.Stats.Weight);
            Assert.Equal(absorb, arm.Stats.Absorb);
            Assert.Equal(resists, arm.Stats.Resistances);
            Assert.Equal(castOnUse, arm.Stats.CastOnUse);
        }
    }
}

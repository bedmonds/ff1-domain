﻿namespace NES.Games.FinalFantasy.Weapons {
    using System;
    using System.Linq;

    /// <summary>
    /// Internal implementation of IWeaponStats.
    /// </summary>
    class WeaponStats : IWeaponStats {
        byte[] raw;

        public WeaponStats(byte[] raw) {
            if (raw.Length != Size) {
                throw new ArgumentException("Weapon stats must be 8 bytes long.");
            }

            this.raw = raw;
        }

        public int Size => 8;

        public byte Accuracy
        {
            get => raw[0];
            set => raw[0] = value;
        }

        public byte Damage
        {
            get => raw[1];
            set => raw[1] = value;
        }

        public byte CriticalHitChance
        {
            get => raw[2];
            set => raw[2] = value;
        }

        // [FIXME] Use actual spell ID
        public byte CastOnUse
        {
            get => raw[3];
            set => raw[3] = value;
        }

        public Element Element
        {
            get => (Element)raw[4];
            set => raw[4] = (byte)value;
        }

        public Genus Bane
        {
            get => (Genus)raw[5];
            set => raw[5] = (byte)value;
        }

        public EffectSprite Sprite
        {
            get => (EffectSprite)raw[6];
            set => raw[6] = (byte)value;
        }

        public Colour Colour
        {
            get => (Colour)raw[7];
            set => raw[7] = (byte)value;
        }

        public BattleAnimation Animation
        {
            get => new BattleAnimation((EffectSprite)raw[6], (Colour)raw[7]);
        }

        public byte[] ToBytes() {
            return
                raw
                    .Take(6)
                    .Concat(new byte[] {
                        (byte)Animation.Sprite,
                        (byte)Animation.Colour
                    })
                    .ToArray();                
        }
    }
}

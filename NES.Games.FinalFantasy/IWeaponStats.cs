namespace NES.Games.FinalFantasy {
    /// <summary>
    /// The item definition for a weapon.
    /// </summary>
    public interface IWeaponStats : IRomValueType {
        /// <summary>
        /// Hit% bonus granted by this weapon.
        /// </summary>
        byte Accuracy { get; set; }

        /// <summary>
        /// Damage bonus granted by this weapon.
        /// </summary>
        byte Damage { get; set; }

        /// <summary>
        /// Chance, out of 200, for a melee swing
        /// with this weapon to be a critical hit.
        /// </summary>
        /// <remarks>
        /// Due to a bug in the vanilla game, this value
        /// will be ignored unless an external patch
        /// fixing the bug is applied.
        /// </remark>
        byte CriticalHitChance { get; set; }

        /// <summary>
        /// Index of the spell cast when this weapon is used in battle.
        /// </summary>
        byte CastOnUse { get; set; }

        /// <summary>
        /// Elemental properties of this weapon.
        /// </summary>
        /// <remarks>
        /// Due to a bug in the vanilla game, these will
        /// not influence damage dealt unless an external
        /// patch is applied that resolves the issue.
        /// </remarks>
        Element Element { get; set; }

        /// <summary>
        /// Mob family bane properties of this weapon.
        /// </summary>
        /// <remarks>
        /// Due to a bug in the vanilla game, these will
        /// not influence damage dealt unless an external
        /// patch is applied that resolves the issue.
        /// </remarks>
        Genus Bane { get; set; }

        /// <summary>
        /// Animation played when swinging this weapon.
        /// </summary>
        BattleAnimation Animation { get; }
    }
}

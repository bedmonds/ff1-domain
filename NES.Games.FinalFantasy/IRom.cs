﻿namespace NES.Games.FinalFantasy {
    using FinalFantasy.Monsters;
    using FinalFantasy.Weapons;

    /// <summary>
    /// Represents a release-agnostic Final Fantasy Rom.
    /// </summary>
    // At some point, adding support for different releases,
    // or supporting useful patches like string expansion,
    // could be an idea.
    public interface IRom {
        byte[] Header { get; }
        byte[] Data { get; }

        bool Valid { get; }

        INamedList<IArmourStats> Armour { get; set; }
        INamedList<IMonsterStats> Monsters { get; set; }
        INamedList<IWeaponStats> Weapons { get; set; }
        IMagic Magic { get; set; }
    }
}

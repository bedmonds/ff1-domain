﻿namespace NES.Games.FinalFantasy.IO {
    using System.Collections.Generic;

    public interface IRomValueList<T> where T : IRomValueType {
        /// <summary>
        /// All of the items in this list.
        /// </summary>
        IEnumerable<T> Items { get; }

        /// <summary>
        /// Retrieve or change a specific item in this list.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        T this[int index] { get; set; }

        /// <summary>
        /// The number of items in this list.
        /// </summary>
        int Count { get; }
    }
}

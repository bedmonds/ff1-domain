﻿// Types that are common across multiple modules.
// For example, elements and status effects.
namespace NES.Games.FinalFantasy {
    using System;

    /// <summary>
    /// A light warrior's character class.
    /// </summary>
    public enum CharacterClass {
        Fighter,
        Thief,
        BlackBelt,
        RedMage,
        WhiteMage,
        BlackMage,
 
        Knight,
        Ninja,
        Master,
        RedWizard,
        WhiteWizard,
        BlackWizard
    }

    /// <summary>
    /// Categorization of elemental attacks and resistances.
    /// </summary>
    /// <remarks>
    /// These can be combined to provide resistance to multiple 
    /// categories at once. In the base game, the White Robe, Black Robe 
    /// and Dragon Armour all use this functionality.
    /// </remarks>
    [Flags]
    public enum Element : byte {
        None = 0x00,

        Status = 0x01,

        // Aegis shield, resists Stone and Poison effects.
        Gas = 0x02,

        // ZAP! and STOP
        Time = 0x04,

        // SQUINT, RUB
        Death = 0x08,

        Fire = 0x10,
        Ice = 0x20,
        Lightning = 0x40,

        Earth = 0x80,

        // Resist everything as the Ribbon does.
        All = 0xFF
    }

    /// <summary>
    /// A monster's type for weapon bane purposes.
    /// </summary>
    /// <remarks>
    /// These can be combined, allowing an 0x06 weapon to be effective
    /// against both Dragons and Giants.
    /// </remarks>
    [Flags]
    public enum Genus : byte {
        None = 0x00,

        // This genus honestly makes no sense.
        // Kary and Lich are magical, but not kraken or Tiamat,
        // All Elementals are, so are MudGOLs but not other golems.
        // It's inconsistent, and only used by the XCalibur with
        // weapon fixes anyway.
        Magical = 0x01,

        // Lizards of all kinds, actually, as this also affects ASPs and COBRAs.
        Dragon = 0x02,

        // Giants, Ogres and Imps.
        Giant = 0x04,

        Undead = 0x08,

        // WrWOLF and MANCAT. Yep, that's it.
        Werebeast = 0x10,

        // SAHAG, SHARK, pretty much anything that's a fish, a lobster,
        // or lives with Sebastian.
        Aquatic = 0x20,

        // SORCEROR, WzOGRE, NAGA, et al.
        SpellCaster = 0x40,

        // SeaTrolls, WrWolves, and a few others.
        Regenerating = 0x80,

        // Xcalbur
        All = 0xFF
    }

    [Flags]
    public enum StatusEffect : byte {
        Death = 0x01,
        Stone = 0x02,
        Poison = 0x04,
        Blind = 0x08,
        Stun = 0x10,
        Sleep = 0x20,
        Silence = 0x40,
        Confuse = 0x80
    }
}

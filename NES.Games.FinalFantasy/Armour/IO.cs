﻿namespace NES.Games.FinalFantasy.Armour {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using NES.Games.FinalFantasy.IO;
    using NES.Games.FinalFantasy.Text;

    class ArmourStatBlock : FixedWidthBlock<IArmourStats> {
        public ArmourStatBlock(int address) : base(address, 4, 40) {}
        
        public ArmourStatBlock(int address, IRom rom) : base(address, 4, 40) {
            Load(rom);
        }

        protected override IArmourStats Build(byte[] buf)
            => new ArmourStats(buf);
    }

    class ArmourPartition : NamedPartition<IArmourStats, ArmourStatBlock> {
        public ArmourPartition(ITextBlock names, ArmourStatBlock stats)
            : base(names, stats) {}
    }
}

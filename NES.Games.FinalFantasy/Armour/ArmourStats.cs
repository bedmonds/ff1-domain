namespace NES.Games.FinalFantasy.Armour {
    using FinalFantasy.IO;

    /// <sumary>
    /// Internal implementation of IArmourStats.
    /// </summary>
    class ArmourStats : FixedWidthRomValue, IArmourStats {
        public ArmourStats(byte[] raw) : base(raw) { }

        public override int Size => 4;

        public byte Weight {
            get => raw[0];
            set => raw[0] = value;
        }

        public byte Absorb {
            get => raw[1];
            set => raw[1] = value;
        }

        public Element Resistances {
            get => (Element)raw[2];
            set => raw[2] = (byte)value;
        }

        public byte CastOnUse {
            get => raw[3];
            set => raw[3] = value;
        }
    }
}

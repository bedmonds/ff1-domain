﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NES.Games.FinalFantasy.Monsters {
    /// <summary>
    /// Represents a status effect applied when a monster
    /// uses a melee attack against a character.
    /// </summary>
    public enum OnHitEffect : byte {
        None = 0,
        Death = 0x01,
        Petrify = 0x02,
        Poison = 0x04,
        Blind = 0x08,
        Stun = 0x10,
        Sleep = 0x20
    }

    /// <summary>
    /// Represents how often an on-hit effect should trigger.
    /// </summary>
    // [TODO] Figure out actual odds.
    public enum OnHitFrequency : byte {
        Never,
        Rare,
        Often
    }
}

﻿namespace NES.Games.FinalFantasy {
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A rom value that has an associated name.
    /// </summary>
    /// <typeparam name="T">
    /// Rom value type that has a name. For example, "WeaponStats"
    /// </typeparam>
    public interface INamed<T> where T : IRomValueType {
        string Name { get; set; }
        T Stats { get ; set; }
    }

    /// <summary>
    /// A collection of named rom values.
    /// </summary>
    /// <typeparam name="T">
    /// Rom value type that has a name. For example, "MonsterStats"
    /// </typeparam>
    public interface INamedList<T>
        where T : IRomValueType
    {
        /// <summary>
        /// The number of items in this collection.
        /// </summary>
        int Count { get; }
        
        /// <summary>
        /// Retrieve or update a specific item in this collection.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        INamed<T> this[int index] { get; set; }

        IEnumerator<INamed<T>> GetEnumerator();
    }
}

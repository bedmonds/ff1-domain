﻿namespace NES.Games.FinalFantasy {
    public enum EffectSprite : byte {
        ShortSword = 0x80,
        Scimitar = 0x84,
        Falchon = 0x88,
        LongSword = 0x8C,
        Sabre = 0x90,
        Hammer = 0x94,
        Knife = 0x98,
        Axe = 0x9C,
        Staff = 0xA0,
        Rod = 0xA4,
        Nunchuck = 0xA8,
        Fist = 0xAC,

        // Magic
        LightPillar = 0xB0,
        Sparks = 0xB8,
        Stars = 0xC0,
        Beam = 0xC8,
        Flare = 0xD0,
        Ball = 0xD8,
        LargeSpark = 0xE0,
        Glow = 0xE8,
    }

    public enum Colour : byte {
        LightGrey = 0x20,
        Blue = 0x21,
        Aqua = 0x22,
        Purple = 0x23,
        LightPink = 0x24,
        DarkPink = 0x25,
        Orange = 0x26,
        Gold = 0x27,
        Yellow = 0x28,
        GreenYellow = 0x29,
        Green = 0x2A,
        Aquamarine = 0x2B,
        Cyan = 0x2C,
        DarkGrey = 0x2D
    }

    /// <summary>
    /// Animation used by the MAGIC or FIGHT commands.
    /// </summary>
    public struct BattleAnimation {
        /// <summary>
        /// Sprite used by this animation.
        /// </summary>
        public EffectSprite Sprite { get; set; }

        /// <summary>
        /// Colour of this animation's sprite.
        /// </summary>
        public Colour Colour { get; set; }

        public BattleAnimation(EffectSprite sprite, Colour colour) {
            Sprite = sprite;
            Colour = colour;
        }
    }
}

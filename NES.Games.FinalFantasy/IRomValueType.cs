﻿namespace NES.Games.FinalFantasy {
    /// <summary>
    /// Represents a fixed size piece of Final Fantasy data.
    /// For example, Monster stats or spell definitions.
    /// </summary>
    public interface IRomValueType {
        int Size { get; }
        byte[] ToBytes();
    }
}

﻿namespace NES.Games.FinalFantasy.Roms {
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text;

    using FinalFantasy.Armour;
    using FinalFantasy.IO;
    using FinalFantasy.Monsters;
    using FinalFantasy.Text;
    using FinalFantasy.Weapons;

    public class FFUsa : IRom {
        public byte[] Header { get; }
        public byte[] Data { get; }

        public FFUsa(string path) {
            using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                Header = new byte[fs.Length % 8192];
                Data = new byte[fs.Length - Header.Length];

                fs.Read(Header, 0, Header.Length);
                fs.Read(Data, 0, Data.Length);
            }

            Initialize();
        }

        public FFUsa(byte[] data) {
            using (var ms = new MemoryStream(data)) {
                Header = new byte[data.Length % 8192];
                Data = new byte[data.Length - Header.Length];

                ms.Read(Header, 0, Header.Length);
                ms.Read(Data, 0, Data.Length);
            }

            Initialize();
        }

        PartitionTable Partitions { get; set; }

        void Initialize() {
            Partitions = new PartitionTable(this);

            Partitions.RegisterNamed(new MonsterPartition(
                Text[FFStringGroup.EnemyNames],
                new MonsterStatBlock(0x30520)
            ));

            Partitions.RegisterNamed(new WeaponPartition(
                Text[FFStringGroup.StandardText].SubSection(27, 40),
                new WeaponStatBlock(0x30000)
            ));

            Partitions.RegisterNamed(new ArmourPartition(
                Text[FFStringGroup.StandardText].SubSection(67, 40),
                new ArmourStatBlock(0x30140)
            ));
            // Pointers are 8 bytes at 0x3AD10
            spellPermissions = new SpellPermissionBlock(0x3AD18, this);
        }

        Dictionary<FFStringGroup, TextBlock> text;
        Dictionary<FFStringGroup, TextBlock> Text {
            get {
                if (text != null) return text;

                text = new Dictionary<FFStringGroup, TextBlock> {
                   { FFStringGroup.Dialogue, new TextBlock(0x20000, 0x28000, 0x28200, 0x2B5FF, 256, this) },
                   { FFStringGroup.EnemySkills, new TextBlock(0x20000, 0x2B600, 0x2B634, 0x2B6E5, 26, this) },
                   { FFStringGroup.StandardText, new TextBlock(0x20000, 0x2B702, 0x2B901, 0x2BFA3, 251, this) },
                   { FFStringGroup.BattleMessages, new TextBlock(0x24000, 0x2CF50, 0x2CC40, 0x2CF50, 78, this) },
                   { FFStringGroup.EnemyNames, new TextBlock(0x24000, 0x2D4E0, 0x2D5E0, 0x2D947, 128, this) },
                };

                return text;
            }
        }

        public INamedList<IMonsterStats> Monsters {
            get => Partitions.GetNamed<IMonsterStats>();
            set => Partitions.UpdateNamed(value);
        }
        
        public INamedList<IWeaponStats> Weapons {
            get => Partitions.GetNamed<IWeaponStats>();
            set => Partitions.UpdateNamed(value);
        }

        public INamedList<IArmourStats> Armour {
            get => Partitions.GetNamed<IArmourStats>();
            set => Partitions.UpdateNamed(value);
        }

        SpellPermissionBlock spellPermissions;
        public IEnumerable<SpellPermission> SpellPermissions {
            get => spellPermissions.Items;
            set {
                spellPermissions.Items = value;
                spellPermissions.Save(this);
            }
        }

        public bool Valid
            => Data.Take(16).SequenceEqual(new byte[] {
                0x06, 0x40, 0x0E, 0x89, 0x0E, 0x89, 0x0E, 0x40,
                0x1E, 0x40, 0x0E, 0x40, 0x0E, 0x40, 0x0B, 0x42
            });
    }
}



﻿namespace NES.Games.FinalFantasy.IO {
    /// <summary>
    /// Metadata for a block of related information in a ROM.
    /// </summary>
    /// <remarks>
    /// For example, weapons, armour, spell names.
    /// </remarks>
    interface IRomBlock<T> where T : IRomValueType {
        /// <summary>
        /// Starting address for this block's data.
        /// The header bytes are ignored, which means that
        /// address 0 represents byte {HeaderLength} + 0.
        /// </summary>
        int StartAddress { get; }

        /// <summary>
        /// The address of the last byte of this block.
        /// </summary>
        /// <remarks>
        /// While some blocks, such as weapon stats, are fixed width,
        /// others have a variable-width with bytes to spare. We need
        /// a way to ensure that var-width blocks don't spill over into
        /// other blocks' data.
        /// </remarks>
        int EndAddress { get; }

        /// <summary>
        /// Number of items in this block.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Total number of bytes required by the entire block.
        /// </summary>
        int Size { get; }
    }
}

﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A composite block, where data is referenced to by a pointer
    /// table.
    /// </summary>
    /// <typeparam name="T">ROM value type of this block's data</typeparam>
    /// <typeparam name="TDataBlock">Data block type</typeparam>
    abstract class AddressedBlock<T, TDataBlock>
        : IRomValueList<T>, IRomSerializable
        where T : IRomValueType
        where TDataBlock : IRomValueList<T>, IRomSerializable {
        public AddressedBlock(
            int pointerOffset,
            int pointersStart,
            TDataBlock data,
            int count
        ) {
            Pointers = new PointerBlock(pointersStart, pointerOffset, count);
            Data = data;
        }

        public T this[int index]
        {
            get => Data[index];
            set => throw new NotImplementedException();
        }
        public IEnumerable<T> Items => Data.Items;

        PointerBlock Pointers { get; }
        TDataBlock Data { get; }
        public int Count => Pointers.Count;
        public bool Loaded => Pointers.Loaded && Data.Loaded;

        public void Load(IRom rom) {
            Pointers.Load(rom);
            Data.Load(rom);
        }

        public void Save(IRom rom) {
            throw new NotImplementedException();
        }
    }
}

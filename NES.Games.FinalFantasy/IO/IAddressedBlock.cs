﻿namespace NES.Games.FinalFantasy.IO {
    /// <summary>
    /// Represents a block of data whose items are referenced by a 
    /// block of pointers.
    /// </summary>
    interface IAddressedBlock<T> where T : IRomValueType {
        /// <summary>
        /// Load this block's entries and pointers from a ROM.
        /// </summary>
        /// <param name="rom"></param>
        void Load(IRom rom);

        /// <summary>
        /// Write this block's entries and pointers to a ROM.
        /// </summary>
        /// <param name="rom"></param>
        void Write(IRom rom);

        /// <summary>
        /// Pointers to each item in Data. Pointers are addresses
        /// relative to the page this block is in and not
        /// absolute addresses from the start of the ROM.
        /// </summary>
        PointerBlock Pointers { get; }

        IRomBlock<T> Data { get; }
    }
}

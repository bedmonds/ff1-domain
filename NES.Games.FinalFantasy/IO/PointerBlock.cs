﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a block of relative-address pointers.
    /// </summary>
    class PointerBlock : IRomBlock<Pointer>, IRomSerializable {
        int PointerOffset { get; }

        public int Count { get; }
        public int StartAddress { get; }
        public IEnumerable<Pointer> Entries { get; protected set; }
        public bool Loaded { get; private set; }

        public PointerBlock(int startAddress, int pointerOffset, int count) {
            StartAddress = startAddress;
            PointerOffset = pointerOffset;
            Count = count;
        }

        public PointerBlock(int startAddress, int pointerOffset, int count, IRom rom)
            : this(startAddress, pointerOffset, count) {
            Load(rom);
        }

        public int EndAddress => StartAddress + Size;
        public int Size => Count * ItemSize;
        int ItemSize => 2;


        public void Load(IRom rom) {
            Entries = Rom.ReadItems(
                rom,
                StartAddress,
                ItemSize,
                Count,
                (buf) => new Pointer(buf)
            );

            Loaded = true;
        }

        public void Save(IRom rom) {
            throw new NotImplementedException();
        }
    }
}

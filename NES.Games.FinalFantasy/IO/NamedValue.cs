﻿namespace NES.Games.FinalFantasy.IO {
    using FinalFantasy.Text;

    public class NamedValue<T> : INamed<T>
        where T : IRomValueType
    {
        FFString internalName;

        public NamedValue(int id, FFString name, T stats) {
            Id = id;
            internalName = name;
            Stats = stats;
        }

        public int Id { get; }
        public string Name {
            get => internalName.Value;
            set => internalName.Value = value;
        }
        public T Stats { get; set; }
    }
}

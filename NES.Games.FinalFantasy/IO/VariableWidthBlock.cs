﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;
    using System.Linq;

    enum SeparationMode {
        // New items are defined by a separator byte.
        // (e.g. text data where items are separated by 0x00)
        NewItemOnSeparator,

        // Items can be either a single byte or
        // a single byte + mask identifying a sequence of up
        // to 255 items, the number of items in the sequence
        // corresponding to the following byte.
        // (e.g. Map data)
        Sequence,
    }

    class SeparationBehaviour {
        public SeparationMode Mode { get; }
        public byte Separator { get; }

        public SeparationBehaviour(
            byte separator,
            SeparationMode mode = SeparationMode.NewItemOnSeparator
        ) {
            Mode = mode;
            Separator = separator;
        }

        public static SeparationBehaviour StandardText
            => new SeparationBehaviour(0x00, SeparationMode.NewItemOnSeparator);
    }

    abstract class VariableWidthBlock<T>
        : IRomBlock<T>, IRomSerializable, IRomValueList<T>
        where T : IRomValueType {
        protected abstract T Build(byte[] buf);

        int initialCount;
        SeparationBehaviour behaviour;

        public VariableWidthBlock(
            int address,
            int endAddress,
            int initialCount,
            SeparationBehaviour behaviour
        ) {
            if (address < 0) {
                throw new ArgumentException("Starting address must be a positive integer");
            }

            if (initialCount < 1 || initialCount < 1) {
                throw new ArgumentException("Initial item count must be positive non-zero");
            }

            StartAddress = address;
            EndAddress = endAddress;
            this.initialCount = initialCount;
            this.behaviour = behaviour;
        }

        public int StartAddress { get; }
        public int EndAddress { get; }
        public int Count => items?.Count ?? initialCount;
        public int Size => Items.Sum(i => i.Size);
        public bool Loaded { get; private set; }

        List<T> items;
        public IEnumerable<T> Items
        {
            get => items;
            set => items = new List<T>(value);
        }

        public T this[int index]
        {
            get => items[index];
            set => items[index] = value;
        }

        int MaxLength => EndAddress - StartAddress;

        public void Load(IRom rom) {
            switch (behaviour.Mode) {
            case SeparationMode.NewItemOnSeparator:
                Items = Rom.ReadVariableWidthItems(
                    rom,
                    StartAddress,
                    MaxLength,
                    Count,
                    behaviour.Separator,
                    Build
                );
                break;
            case SeparationMode.Sequence:
                // LoadSequence(rom);
                break;
            default:
                throw new NotImplementedException($"No implementation found for the mode {behaviour.Mode}");
            }
            Loaded = true;
        }

        public void Save(IRom rom) {
            switch (behaviour.Mode) {
            case SeparationMode.NewItemOnSeparator:
                // SaveSeparated(rom);
                break;
            case SeparationMode.Sequence:
                // SaveSequence(rom);
                break;
            default:
                throw new NotImplementedException($"No implementation found for the mode {behaviour.Mode}");
            }
        }
    }
}

﻿namespace NES.Games.FinalFantasy.IO {
    /// <summary>
    /// Represents some structure that can be loaded from, or saved to,
    /// a Final Fantasy ROM.
    /// </summary>
    interface IRomSerializable {
        bool Loaded { get; }

        /// <summary>
        /// Load the contents of a ROM into this block's Entries.
        /// </summary>
        /// <param name="rom">A Final Fantasy rom.</param>
        void Load(IRom rom);

        /// <summary>
        /// Write the contents of this block's Entries to a ROM.
        /// </summary>
        /// <param name="rom">A Final Fantasy rom.</param>
        void Save(IRom rom);
    }

    interface IRomSerializableList<T>
        : IRomValueList<T>, IRomSerializable
        where T : IRomValueType { }

    interface IRomSerializableNamedList<T>
        : INamedList<T>, IRomSerializable
        where T : IRomValueType { }
}

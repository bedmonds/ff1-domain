﻿namespace NES.Games.FinalFantasy.IO {
    interface IBuilder<T> where T : IRomValueType {
        T Build();
    }
}

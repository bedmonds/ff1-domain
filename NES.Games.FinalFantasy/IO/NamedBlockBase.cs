﻿namespace NES.Games.FinalFantasy.IO {
    using System.Collections.Generic;
    using System.Linq;

    using FinalFantasy.Text;

    abstract class NamedPartition<T, TDataBlockType>
        : IRomSerializableNamedList<T>
        where T : IRomValueType
        where TDataBlockType : IRomValueList<T>, IRomSerializable {

        ITextBlock Names { get; }
        TDataBlockType Stats { get; }
        public bool Loaded => Names.Loaded && Stats.Loaded;

        INamed<T> Build(int id, FFString name, T stats)
            => new NamedValue<T>(id, name, stats);

        public NamedPartition(
            ITextBlock names,
            TDataBlockType stats
        ) {
            Names = names;
            Stats = stats;
        }

        public int Count => Stats.Items.Count();

        public INamed<T> this[int index]
        {
            get => Build(index, Names[index], Stats[index]);
            set
            {
                Names[index].Value = value.Name;
                Stats[index] = value.Stats;
            }
        }

        public IEnumerator<INamed<T>> GetEnumerator() {
            for (int i = 0; i < Stats.Items.Count(); ++i) {
                yield return Build(i, Names[i], Stats[i]);
            }
        }

        public void Load(IRom rom) {
            Names.Load(rom);
            Stats.Load(rom);
        }

        public void Save(IRom rom) {
            // [FIXME] Rewrite pointers
            Names.Save(rom);
            Stats.Save(rom);
        }
    }
}

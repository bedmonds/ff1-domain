﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NES.Games.FinalFantasy.Text.Encoding {
    /// <summary>
    /// Supplemental characters to the DTETable.
    /// </summary>
    enum Glyph : byte {
        // Indicates the end of a text record.
        // The game uses this inconsistently; some item names have
        // 8 bytes, others 5, others 4.
        Separator = 0x00,

        Sword = 212,
        Hammer,
        Dagger,
        Axe,
        Staff,
        Nunchuck,

        Armour,
        Shield,
        Helmet,
        Gauntlet,
        Bracelet,
        Robe,
        Percent,
        Potion,

        Space = 0xFF
    }
}

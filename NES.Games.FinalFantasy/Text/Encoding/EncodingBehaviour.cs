﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NES.Games.FinalFantasy.Text.Encoding {
    public enum Padding {
        None,
        PadLeft,
        PadRight
    }

    public struct Behaviour {
        // Maximum number of bytes of the encoded string.
        public int MaxLength;

        // If and how to pad a string that is shorter than MaxLength.
        public Padding Padding;
        public byte PaddingChar;

        // Encode multi-character bytes using the DTE.
        public bool UseDTETable;

        public Behaviour(
            int maxLength = 0,
            Padding padding = Padding.None,
            byte padWith = 0xFF,
            bool useDTE = true
        ) {
            MaxLength = maxLength;
            Padding = padding;
            PaddingChar = padWith;
            UseDTETable = useDTE;
        }

        public static Behaviour None
            => new Behaviour();

        public static Behaviour FlagString
            => new Behaviour(16, Padding.PadLeft, 0xFF, false);

        public static Behaviour Raw
            => new Behaviour(0, Padding.None, 0xFF, false);

    }
}
